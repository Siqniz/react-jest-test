import Axios from 'axios';

const instance = Axios.create({
	baseURL: 'https://jsonplaceholder.typicode.com/',
});

export async function getPosts(url) {
	const result = await instance.get(url);
	return result;
}

export async function getPostByID(url) {
	const result = await instance.get(url);
	return result;
}
