import axios from 'axios';
import { capitlizeLetter, sumNumbers } from '../../business/business';

import { getPosts, getPostByID } from '../instance';

jest.mock('axios');

test('Should get posts', () => {
	const posts = [
		{ title: 'test', body: 'loreum ipsome' },
		{ title: 'test 1', body: 'loreum ipsome' },
		{ title: 'test 2', body: 'loreum ipsome' },
	];

	axios.get.mockResolvedValue(posts);
	return getPosts().then((data) => expect(data.length).toEqual(3));
});

test('Should get 1 post by id', () => {
	const post = { title: 'Test', body: 'test' };
	axios.get.mockResolvedValue(post);

	return getPostByID(2).then((data) =>
		expect(post).toMatchObject({ title: 'Test', body: 'test' })
	);
});

test('Should have title', () => {
	const post = { title: 'Test', body: 'test' };
	axios.get.mockResolvedValue(post);

	return getPostByID(2).then((data) => expect(data).toHaveProperty('title')); //
});

test('SHould cap letter', () => {
	const post = { title: 'Test', body: 'test' };
	axios.get.mockResolvedValue(post);

	return getPostByID(2).then((data) => {
		expect(capitlizeLetter(data.title)).toBe('TEST');
	});
});

test('Should Sum 2 numbers', () => {
	expect(4 + 1097434).toBe(1097438);
});

test('Should be typeof number', () => {
	expect(typeof (4 + 1097434)).toBe('number');
});
