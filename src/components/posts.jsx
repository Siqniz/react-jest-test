import React, {useEffect, useState} from 'react';
import {getPosts} from '../service/instance'
import Post from './post'



const Posts  = () =>{

    const [_posts, setPost] = useState([]);

    useEffect(()=>{
       getPosts('/posts').then((resp)=>{
            const {data} = resp;
            setPost(data);
        });
 
    },[setPost])

    return(
        <div className='posts'>
            {_posts.map(p=><Post post={p} key={p.id}></Post>)}
        </div>
    )
}

export default Posts