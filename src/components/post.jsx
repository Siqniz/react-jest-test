import React from 'react';


const Post= (props) =>{
    return(
        <div className='post'>
           <h2>{props.post.title}</h2>
            <div>{props.post.body}</div>
        </div>
    )
}

export default Post;